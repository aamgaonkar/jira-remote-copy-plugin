package com.atlassian.cpji.bulk;

import org.apache.log4j.Logger;

import com.atlassian.jira.bulkedit.operation.BulkOperationException;
import com.atlassian.jira.bulkedit.operation.ProgressAwareBulkOperation;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.BulkEditBean;

public class CustomBulkOperation implements ProgressAwareBulkOperation {

	private static final Logger log = Logger
			.getLogger(CustomBulkOperation.class);

	@Override
	public String getDescriptionKey() {
		String descriptionKey = "bulk.operation.description";
		log.debug("descriptionKey: " + descriptionKey);
		return descriptionKey;
	}

	@Override
	public String getNameKey() {
		String nameKey = this.getClass().getName();
		log.debug("nameKey: " + nameKey);
		return nameKey;
	}

	@Override
	public boolean canPerform(BulkEditBean arg0, ApplicationUser arg1) {
		boolean canPerform = true;
		log.debug("canPerform: " + canPerform);
		int totalSelectedIssues = arg0.getSelectedIssues().size();
		log.debug("In CustomBulkOperation.canPerform(), totalSelectedIssues: " + totalSelectedIssues);
		return canPerform;
	}

	@Override
	public String getCannotPerformMessageKey() {
		String cannotPerformMessageKey = "bulk.operation.cannotperformmessage";
		log.debug("cannotPerformMessageKey: " + cannotPerformMessageKey);
		return cannotPerformMessageKey;
	}

	@Override
	public int getNumberOfTasks(BulkEditBean arg0) {
		int numberOfTasks = arg0.getSelectedIssues().size();
		log.debug("numberOfTasks: " + numberOfTasks);
		return numberOfTasks;
	}

	@Override
	public String getOperationName() {
		String operationName = "BulkOperation";
		log.debug("operationName: " + operationName);
		return operationName;
	}

	@Override
	public void perform(BulkEditBean bulkEditBean,
						ApplicationUser applicationUser,
						Context context) throws BulkOperationException {
		log.debug("In Custom Bulk Operation perform");
	}
}
