package com.atlassian.cpji.bulk;

import org.apache.log4j.Logger;

import webwork.action.ActionContext;

import com.atlassian.jira.web.SessionKeys;
import com.atlassian.jira.web.bean.BulkEditBean;
import com.atlassian.jira.web.bean.BulkEditBeanSessionHelper;

public class CustomBulkEditBeanSessionHelper extends BulkEditBeanSessionHelper
{
	private static final Logger log = Logger.getLogger(CustomBulkEditBeanSessionHelper.class);

    @SuppressWarnings("unchecked")
	public void storeToSession(final BulkEditBean bulkEditBean)
    {
    	log.debug("Storing Bulk Edit Bean to Session.");
        ActionContext.getSession().put(SessionKeys.BULKEDITBEAN, bulkEditBean);
    }

    public BulkEditBean getFromSession()
    {
    	log.debug("Getting Bulk Edit Bean from Session.");
        return (BulkEditBean) ActionContext.getSession().get(SessionKeys.BULKEDITBEAN);
    }

    public void removeFromSession()
    {
    	log.debug("Removing Bulk Edit Bean from Session.");
        ActionContext.getSession().remove(SessionKeys.BULKEDITBEAN);
    }
}