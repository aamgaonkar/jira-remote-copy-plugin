package com.atlassian.cpji.bulk;

import org.apache.log4j.Logger;

import com.atlassian.jira.bulkedit.BulkOperationManager;
import com.atlassian.sal.api.lifecycle.LifecycleAware;

public class CustomBulkInjectorImpl implements CustomBulkInjector,
		LifecycleAware {

	private static final Logger log = Logger.getLogger(CustomBulkInjectorImpl.class);
	private final BulkOperationManager bulkOperationManager;

	CustomBulkInjectorImpl(BulkOperationManager bulkOperationManager) {
		this.bulkOperationManager = bulkOperationManager;
	}

	@Override
	public void onStart() {
		log.debug("onStart(): Adding custom progress aware bulk operation to bulkOperationManager");
		bulkOperationManager.addProgressAwareBulkOperation("com.atlassian.cpji.bulk.CustomBulkOperation",CustomBulkOperation.class);
	}
}