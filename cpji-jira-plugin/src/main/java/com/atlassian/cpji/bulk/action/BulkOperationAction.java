package com.atlassian.cpji.bulk.action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.cpji.action.SelectedProject;
import com.atlassian.cpji.components.CopyIssuePermissionManager;
import com.atlassian.cpji.components.model.NegativeResponseStatus;
import com.atlassian.cpji.components.remote.JiraProxy;
import com.atlassian.cpji.components.remote.JiraProxyFactory;
import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.exception.IssueNotFoundException;
import com.atlassian.jira.exception.IssuePermissionException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.web.action.issue.bulkedit.AbstractBulkOperationAction;
import com.atlassian.jira.web.bean.BulkEditBean;
import com.atlassian.jira.web.bean.BulkEditBeanSessionHelper;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;

public abstract class BulkOperationAction extends AbstractBulkOperationAction {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(BulkOperationAction.class);

	public static final String AUTHORIZE = "authorize";
	public static final String PLUGIN_KEY = "com.atlassian.cpji.cpji-jira-plugin";

	protected String targetEntityLink;

	protected final FieldLayoutManager fieldLayoutManager;
	protected final CommentManager commentManager;
	protected final ApplicationLinkService applicationLinkService;
	protected final JiraProxyFactory jiraProxyFactory;

	private String genericResponseHandlerResult = ERROR;
	private String authorizationUrl;
	private String currentStep;

	private final CopyIssuePermissionManager copyIssuePermissionManager;
	private final IssueLinkTypeManager issueLinkTypeManager;
	private final BulkEditBeanSessionHelper bulkEditBeanSessionHelper;

	public BulkOperationAction(final SearchService searchService,
							   final BulkEditBeanSessionHelper bulkEditBeanSessionHelper,
							   final JiraProxyFactory jiraProxyFactory,
							   final CopyIssuePermissionManager copyIssuePermissionManager,
							   final IssueLinkTypeManager issueLinkTypeManager,
							   final PageBuilderService pageBuilderService,
							   final FieldLayoutManager fieldLayoutManager,
							   final CommentManager commentManager,
							   final ApplicationLinkService applicationLinkService) {
		super(searchService, bulkEditBeanSessionHelper);
		this.jiraProxyFactory = jiraProxyFactory;
		this.copyIssuePermissionManager = copyIssuePermissionManager;
		this.issueLinkTypeManager = issueLinkTypeManager;
		this.bulkEditBeanSessionHelper = bulkEditBeanSessionHelper;
		this.fieldLayoutManager = fieldLayoutManager;
		this.commentManager = commentManager;
		this.applicationLinkService = applicationLinkService;

		pageBuilderService.assembler().resources().requireContext("com.atlassian.cpji.cpji-jira-plugin.copy-context");
	}

	public SelectedProject getSelectedDestinationProject() {
		try {
			String[] strings = StringUtils.split(URLDecoder.decode(targetEntityLink, "UTF-8"), "|");
			return new SelectedProject(jiraProxyFactory.getLocationById(strings[0]), strings[1]);
		} catch (UnsupportedEncodingException ex) {
			throw new RuntimeException("UTF-8 encoding not supported", ex);
		}
	}

	@Override
	public BulkEditBean getRootBulkEditBean() {
		BulkEditBean bulkEditBean = null;
		try {
			bulkEditBean = bulkEditBeanSessionHelper.getFromSession();
			log.debug("bean: " + bulkEditBean);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return bulkEditBean;
	}

	public boolean isTargetLocal() {
		return getSelectedDestinationProject().getJiraLocation().isLocal();
	}

	public List<Issue> getSelectedIssues() {
		List<Issue> issueList = null;
		issueList = getRootBulkEditBean().getSelectedIssues();
		log.debug("Selected issues: " + issueList.toString());
		return issueList;
	}

	protected <T> T handleGenericResponseStatus(JiraProxy jira, Either<NegativeResponseStatus, T> response, Function<NegativeResponseStatus, Void> errorOccuredHandler) {
		if (response.isRight()) {
			return response.right().get();
		} else {
			NegativeResponseStatus status = response.left().get();
			log.error(status);
			switch (status.getResult()) {
				case AUTHENTICATION_FAILED:
					addErrorMessage(getText("cpji.errors.authentication.failed"));
					break;
				case AUTHORIZATION_REQUIRED:
					addErrorMessage(getText("cpji.errors.oauth.token.invalid"));
					List<Issue> issueList = getSelectedIssues();
					authorizationUrl = jira.generateAuthenticationUrl(Long.toString(issueList.iterator().next().getId()));
					genericResponseHandlerResult = AUTHORIZE;
					break;
				case PLUGIN_NOT_INSTALLED:
					addErrorMessage(getText("cpji.errors.plugin.not.installed"));
					break;
				case COMMUNICATION_FAILED:
					addErrorMessage(getText("cpji.errors.communication.failed"));
					break;
				case ERROR_OCCURRED:
					if (errorOccuredHandler != null) {
						errorOccuredHandler.apply(status);
					} else {
						addErrorMessage(getText("cpji.errors.error.occured.generic"));
					}
			}
			return null;
		}
	}

	public void setTargetEntityLink(String targetEntityLink) {
		this.targetEntityLink = targetEntityLink;
	}

	public String getTargetEntityLink() {
		return this.targetEntityLink;
	}

	protected String checkPermissions() {
		log.debug("Checking Permissions");
		List<Issue> issueList = getSelectedIssues();
		for (Issue issue : issueList) {
			try {
				if (!copyIssuePermissionManager.hasPermissionForProject(issue.getProjectObject().getKey())) {
					addErrorMessage(getText("cpji.error.no.permission"));
					return PERMISSION_VIOLATION_RESULT;
				}
			} catch (final IssueNotFoundException ex) {
				addErrorMessage(getText("admin.errors.issues.issue.does.not.exist"));
				return ISSUE_NOT_FOUND_RESULT;
			} catch (final IssuePermissionException ex) {
				addErrorMessage(getText("admin.errors.issues.no.browse.permission"));
				return PERMISSION_VIOLATION_RESULT;
			}
		}
		return SUCCESS;
	}

	/* Comma separated selected issue keys */
	public String getIssueKeys() {
		List<String> keyList = new ArrayList<String>();
		List<Issue> issueList = getSelectedIssues();
		for (Issue issue : issueList) {
			keyList.add(issue.getKey());
		}
		return StringUtils.join(keyList, ",");
	}

	/* Comma separated selected issue id's */
	public String getIssueIds() {
		List<String> idList = new ArrayList<String>();
		List<Issue> issueList = getSelectedIssues();
		for (Issue issue : issueList) {
			idList.add(issue.getId().toString());
		}
		return StringUtils.join(idList, ",");
	}

	public String getAuthorizationUrl() {
		return authorizationUrl;
	}

	public String getGenericResponseHandlerResult() {
		return genericResponseHandlerResult;
	}

	public void setCurrentStep(String name) {
		Preconditions.checkNotNull(name);
		this.currentStep = name;
	}

	public String getSelectedIssuesURL() {
		String selectedIssuesURL = "../";
		List<Issue> selectedIssues = getSelectedIssues();
		String includeIssues = selectedIssues.toString().replace("[",	"").replace("]", "");
		String selectedIssueKey = selectedIssues.get(0).getKey();
		selectedIssuesURL = String.format("../browse/%s?jql=issue in (%s)", selectedIssueKey, includeIssues);
		log.debug("selectedIssuesURL: " + selectedIssuesURL);
		return selectedIssuesURL;
	}

	public String getAllIssuesURL() {
		String allIssuesURL = "../";
		String selectedIssueKey = getSelectedIssues().get(0).getKey();
		allIssuesURL = String.format("../browse/%s?jql=", selectedIssueKey);
		log.debug("allIssuesURL: " + allIssuesURL);
		return allIssuesURL;
	}

	public String getCurrentStep() {
		return this.currentStep;
	}

	/**
	 * Returns the issue link type specified by the clone link name in the
	 * properties file or null for none.
	 *
	 * @return the issue link type specified by the clone link name in the
	 *         properties file or null for none.
	 */
	protected IssueLinkType getCloneIssueLinkType() {
		String cloneIssueLinkTypeName = getCloneIssueLinkTypeName();
		if (cloneIssueLinkTypeName == null)
			return null;
		final Collection<IssueLinkType> cloneIssueLinkTypes = issueLinkTypeManager.getIssueLinkTypesByName(cloneIssueLinkTypeName);

		if (cloneIssueLinkTypes.isEmpty()) {
			return null;
		} else {
			return cloneIssueLinkTypes.iterator().next();
		}
	}

	protected String getCloneIssueLinkTypeName() {
		return getApplicationProperties().getDefaultBackedString(APKeys.JIRA_CLONE_LINKTYPE_NAME);
	}
}
