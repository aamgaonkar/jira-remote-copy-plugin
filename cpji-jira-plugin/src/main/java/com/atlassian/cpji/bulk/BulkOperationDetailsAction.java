package com.atlassian.cpji.bulk;

import org.apache.log4j.Logger;

import com.atlassian.jira.bulkedit.BulkOperationManager;
import com.atlassian.jira.bulkedit.operation.ProgressAwareBulkOperation;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.bean.BulkEditBean;
import com.atlassian.jira.web.bean.BulkEditBeanFactory;
import com.atlassian.jira.web.bean.BulkEditBeanSessionHelper;

public class BulkOperationDetailsAction extends JiraWebActionSupport {

	private static final Logger log = Logger.getLogger(BulkOperationDetailsAction.class);
	private static final long serialVersionUID = 7272036213341647146L;
	private final BulkEditBeanFactory bulkEditBeanFactory;
	private final BulkOperationManager bulkOperationManager;
	private final ProgressAwareBulkOperation customBulkOperation;
	private final BulkEditBeanSessionHelper bulkEditBeanSessionHelper;

	BulkOperationDetailsAction(final BulkEditBeanFactory bulkEditBeanFactory,
							   final BulkOperationManager bulkOperationManager,
							   final BulkEditBeanSessionHelper bulkEditBeanSessionHelper) {

		this.bulkEditBeanFactory = bulkEditBeanFactory;
		this.bulkOperationManager = bulkOperationManager;
		this.bulkEditBeanSessionHelper = bulkEditBeanSessionHelper;

		customBulkOperation = this.bulkOperationManager.getProgressAwareOperation(CustomBulkOperation.class.getName());
	}

	public ProgressAwareBulkOperation getCustomBulkOperation() {
		return customBulkOperation;
	}

	public BulkEditBeanFactory getBulkEditBeanFactory() {
		return bulkEditBeanFactory;
	}

	@Override
	public String doDefault() {
		log.debug("doDefault()");
		return INPUT;
	}

	public BulkEditBean getRootBulkEditBean() {
		BulkEditBean bulkEditBean = bulkEditBeanSessionHelper.getFromSession();
		log.debug("getRootBulkEditBean(): " + bulkEditBean);
		return bulkEditBean;
	}

	public String doPerform() {
		log.debug("doPerform()");
		log.debug("Redirecting from BulkOperationsDetailsAction to BulkSelectTargetProject...");
		UrlBuilder builder = new UrlBuilder("/secure/BulkSelectTargetProject!default.jspa?id="+getRootBulkEditBean().getSelectedIssues().toString());
		return getRedirect(builder.asUrlString());
	}
}