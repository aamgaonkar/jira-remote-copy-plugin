package com.atlassian.cpji.bulk.action;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.cpji.action.SelectedProject;
import com.atlassian.cpji.components.CopyIssuePermissionManager;
import com.atlassian.cpji.components.RecentlyUsedProjectsManager;
import com.atlassian.cpji.components.model.JiraLocation;
import com.atlassian.cpji.components.model.NegativeResponseStatus;
import com.atlassian.cpji.components.model.PluginVersion;
import com.atlassian.cpji.components.remote.JiraProxy;
import com.atlassian.cpji.components.remote.JiraProxyFactory;
import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.bean.BulkEditBeanSessionHelper;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class BulkSelectTargetProjectAction extends BulkOperationAction 
{
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(BulkSelectTargetProjectAction.class);
	private final RecentlyUsedProjectsManager recentlyUsedProjectsManager;

    public BulkSelectTargetProjectAction(final SearchService searchService,
										 final BulkEditBeanSessionHelper bulkEditBeanSessionHelper,
										 final JiraProxyFactory jiraProxyFactory,
										 final CopyIssuePermissionManager copyIssuePermissionManager,
										 final IssueLinkTypeManager issueLinkTypeManager,
										 final PageBuilderService pageBuilderService,
										 final FieldLayoutManager fieldLayoutManager,
										 final CommentManager commentManager,
										 final ApplicationLinkService applicationLinkService,
										 final RecentlyUsedProjectsManager recentlyUsedProjectsManager) {
		super(searchService, bulkEditBeanSessionHelper, jiraProxyFactory,
			  copyIssuePermissionManager, issueLinkTypeManager, pageBuilderService,
			  fieldLayoutManager, commentManager, applicationLinkService);

		this.recentlyUsedProjectsManager = recentlyUsedProjectsManager;

		pageBuilderService.assembler().resources().requireWebResource(PLUGIN_KEY + ":selectTargetProjectAction");
		setCurrentStep("selectproject");
	}

	@Override
    public String doDefault() throws Exception
    {
    	log.debug("doDefault()");
        final String result = checkPermissions();
		if (SUCCESS.equals(result) && recentlyUsedProjectsManager.getRecentProjects(getLoggedInUser()).isEmpty()) {
			recentlyUsedProjectsManager.addProject(getLoggedInUser(), new SelectedProject(JiraLocation.LOCAL, getSelectedIssueProjectKey()));
		}
		return result;
    }

    @Override
    @RequiresXsrfCheck
    public String doExecute() throws Exception
    {
    	log.debug("doExecute()");
		String permissionCheck = checkPermissions();
        if (!permissionCheck.equals(SUCCESS))
        {
            return permissionCheck;
        }
        final SelectedProject selectedEntityLink = getSelectedDestinationProject();
        JiraProxy jira = jiraProxyFactory.createJiraProxy(selectedEntityLink.getJiraLocation());
        Either<NegativeResponseStatus,PluginVersion> response = jira.isPluginInstalled();
        PluginVersion result = handleGenericResponseStatus(jira, response, null);
        if(result == null) {
            return getGenericResponseHandlerResult();
        }

		recentlyUsedProjectsManager.addProject(getLoggedInUser(), selectedEntityLink);
        return getRedirect("/secure/BulkCopyDetailsAction!default.jspa?id=[" + getIssueIds() + "]&targetEntityLink=" + targetEntityLink);
    }

    public String getSelectedIssueProjectKey(){
    	String projectKey = getSelectedIssues().get(0).getProjectObject().getKey();
    	log.debug("In BulkSelectTargetProjectAction.getSelectedIssueProjectKey(): "+projectKey);
        return projectKey;
    }

	public String getRecentlyUsedProjects() {
		final List<SelectedProject> recentProjects = recentlyUsedProjectsManager.getRecentProjects(getLoggedInUser());
		final Map<String, List<String>> model = Maps.newHashMap(); // not using Multimap here because I'd have to install mapper for jackson
		for (SelectedProject project : recentProjects) {
			final String locationId = project.getJiraLocation().getId();
			if (!model.containsKey(locationId)) {
				model.put(locationId, Lists.<String>newArrayList());
			}
			model.get(locationId).add(project.getProjectKey());
		}
		try {
			return new ObjectMapper().writeValueAsString(model);
		} catch (IOException e) {
			return "{}";
		}
	}

}
